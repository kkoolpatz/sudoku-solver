// Global variables begin here 

var canvas = document.getElementById("myCanvas");
var sudoku = document.getElementById("sudoku");
var changeMarkButton = document.getElementById("changeMark");
var changePlayOrderButton = document.getElementById("changePlayOrder");
var ctx = canvas.getContext("2d");
canvas.width = Math.min(window.innerHeight, window.innerWidth) * 0.7;
canvas.height = canvas.width + 50;

var mouseX;
var mouseY;

// In-Game global Variables begin here

p1 = '607410090010390600090620015200751083700839064083200051170542039040980170839076540'
p2 = '400000805030000000000700000020000060000080400000010000000603070500200000104000000'
p3 = '002980500400070013039604070200056400840300201907001086600705130091400005020030608'
p4 = '003020600900305001001806400008102900700000008006708200002609500800203009005010300'

function Game(string) {
    
    this.updateTable = function (board) {
        console.log("entered update Table function")
        for (i = 0; i < 9; i++) {
            for (j = 0; j < 9; j++) {
                var td = document.getElementById("r" + i + "c" + j);
                if (td.textContent == "") {
                    var cell = document.createTextNode(board[i][j]);
                    console.log("updating: " + cell)
                    td.appendChild(cell);
                }
            }
        }
        return "Update Table Complete";
    }

    this.solve = function () {
        solvedBoard = crosshatch(clone(this.board))  
        if (solvedBoard[1]){
            console.log("board Solved")
            this.updateTable(clone(solvedBoard[0]));    
        } else {
            console.log("board not Solved")
        }
        console.log(this.board);
        console.log(solvedBoard[0]);
    }
    
    this.load = function (string){
        var board = [];
        var counter = 0;
        var table = document.createElement("TABLE");
        table.setAttribute("id", "myTable");
        sudoku.appendChild(table);
        
        for (i = 0; i < 9; i++) {
            var row = [];
            var tr = document.createElement("TR");
            table.appendChild(tr);
            for (j = 0; j < 9; j++) {
                var td = document.createElement("TD");
                var cssClass = "r"+ i + "c"+j + " cell";
                if (j % 3 == 0) cssClass += " lbb";
                if (i % 3 == 0) cssClass += " tbb";
                if (string[counter] != 0) {
                    var cell = document.createTextNode(string[counter]);
                    cssClass += " text-green";
                } else {
                    var cell = document.createTextNode("");
                }
                td.setAttribute("class", cssClass);
                td.setAttribute("id", "r" + i + "c" + j);
                tr.appendChild(td);
                td.appendChild(cell);
                console.log(typeof (Number(string[counter])));
                row.push(Number(string[counter]));
                counter++;                
            }
            board.push(row);
        }
        return board;
    }

    this.board = this.load(string);
}



var game = new Game(p4);

function solve() {
    game.solve();
}

