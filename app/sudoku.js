

// Steps
// get possible numbers for each cell
    // in a set add all rows, cols and grid numbers
    // whatever is not there in the set is a possible number
    // if there is only one possibility, then fill it. 
    // if a number was filled, mark the flag to loop 
    // if the whole board is filled, game is over.
// if you are here, then whole board did not fill. 
    // enter backtrace: For each cell, get possible nos
    // pick a number for that cell and recurse. 
        // if game filled, it worked. 
        // else fill the next number and recurse

function crosshatch(board) {
    var crosshatch_loop = true;
    var solved = false;
    while (crosshatch_loop && solved == false) {
        crosshatch_loop = false;
        solved = true;
        for (var row = 0; row < 9; row++) {
            for (var col = 0; col < 9; col++) {
                if (board[row][col] == 0) {
                    var possibleNums = getAllPossibleNums(row, col, board);
                    if (possibleNums.length == 1) {
                        board[row][col] = possibleNums[0];
                        crosshatch_loop = true;
                    }
                    solved = false;
                }
            }
        }
    }
    return [ board, solved ];
}

function getAllPossibleNums(row, col, board) {
    var filledNums = getAllFilledNums(row, col, board);
    var possibleNums = [];
    for (var num = 1; num <= 9; num++) {
        if (!filledNums.includes(num)) {
            possibleNums.push(num)
        }
    }
    return possibleNums;
}

function getAllFilledNums(row, col, board){
    return [...new Set([
        ...getAllFilledGridNums(row, col, board),
        ...getAllFilledRowNums(row, board),
        ...getAllFilledColNums(col, board)
    ])];
}

function getAllFilledRowNums(row, board) {
    var allFilledRowNums = [...new Set(board[row])]
    var index = allFilledRowNums.indexOf(0);
    if (index != -1) {
        allFilledRowNums.splice(index, 1);
    }
    
    console.log(
        "row: " + row
        + ", " + "allFilledRowNums: " + allFilledRowNums
        + ", " + "0index: " + index
    )
    return allFilledRowNums
}


function getAllFilledColNums(col, board) {
    var allFilledColNums = [];
    for (var num = 1; num < 9; num++) {
        if (board[num][col] !== 0) {
            allFilledColNums.push(board[num][col])
        }
    }
    return allFilledColNums;
}


function getAllFilledGridNums(row, col, board) {
    var gridRow = Math.floor(row / 3);
    var gridCol = Math.floor(col / 3);
    var allFilledGridNums = [];
    var gridRowStart = gridRow * 3;
    var gridRowEnd = gridRowStart + 3;
    var gridColStart = gridCol * 3;
    var gridColEnd = gridColStart + 3;
    for (var i = gridRowStart; i < gridRowEnd; i++) {
        for (var j = gridColStart; j < gridColEnd; j++) {
            if (board[i][j] !== 0)
                allFilledGridNums.push(board[i][j]);
        }
    }
    return allFilledGridNums;
}
